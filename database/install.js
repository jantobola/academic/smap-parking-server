var username = "parking-app";
var password = "admin";

print("------------------------------------------------------------------------------------");
print("PART 1: Create Users");
print("------------------------------------------------------------------------------------");

db.createUser({  
   user:username,
   pwd:password,
   roles:[  
      {  
         role: "readWrite",
         db: "parking-app"
      },
      { 
      	 role: "userAdminAnyDatabase",
      	 db: "admin" 
      },
      {  
         role: "userAdmin",
         db: "admin"
      },
      {  
         role: "dbAdmin",
         db: "admin"
      },
      {  
         role: "clusterAdmin",
         db: "admin"
      }
   ]
});

print("Users created.");
print("------------------------------------------------------------------------------------");