package com.jantobola.parking.entity;

import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * ParkingEntity
 *
 * @author Jan Tobola, 2016
 */
public class ParkingEntity {

    @Id
    private String id;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date parkingTime;
    private Double lat;
    private Double lon;
    private String deviceName;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public Date getParkingTime() {
        return parkingTime;
    }

    public void setParkingTime(final Date parkingTime) {
        this.parkingTime = parkingTime;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(final Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(final Double lon) {
        this.lon = lon;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(final String deviceName) {
        this.deviceName = deviceName;
    }

//    @Override
//    public String toString() {
//        StringBuilder b = new StringBuilder();
//
//        b.append("{id: "); b.append(id); b.append(", ");
//        b.append("parkingTime: "); b.append(parkingTime.toString()); b.append(", ");
//        b.append("lat: "); b.append(lat); b.append(", ");
//        b.append("lon: "); b.append(lon); b.append("}");
//
//        return b.toString();
//    }
}
