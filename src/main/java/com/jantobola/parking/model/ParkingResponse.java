package com.jantobola.parking.model;

/**
 * ParkingResponse
 *
 * @author Jan Tobola, 2016
 */
public class ParkingResponse {

    private String message;

    public ParkingResponse(final String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

}
