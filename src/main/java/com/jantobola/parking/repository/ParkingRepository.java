package com.jantobola.parking.repository;

import com.jantobola.parking.entity.ParkingEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * ParkingRepository
 *
 * @author Jan Tobola, 2016
 */
public interface ParkingRepository extends MongoRepository<ParkingEntity, String> {

}