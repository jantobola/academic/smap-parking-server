package com.jantobola.parking.controller.service;

import com.jantobola.parking.entity.ParkingEntity;
import com.jantobola.parking.model.ParkingResponse;
import com.jantobola.parking.repository.ParkingRepository;
import com.sun.deploy.net.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * ServiceController
 *
 * @author Jan Tobola, 2016
 */
@RestController
@RequestMapping("/api")
public class ServiceController {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private ParkingRepository parkingRepository;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ParkingResponse sendCoords(@ModelAttribute ParkingEntity entity) {

        if(entity != null) {

            ParkingEntity savedEntity = parkingRepository.save(entity);

            if(savedEntity != null || savedEntity.getId() != null) {
                log.info("@(green): Saved new parking entity: {}", savedEntity.toString());
                return new ParkingResponse("Parking location saved.");
            }
        }

        return null;
    }

}
